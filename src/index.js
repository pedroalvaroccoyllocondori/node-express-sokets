const express = require('express')
const path = require('path')
const socketio= require('socket.io')
const http=require('http')
const aplicacion = express()

aplicacion.set('port',2000)//configuracion del puerto
aplicacion.use(express.static(path.join(__dirname,'public')))//configuracion del middleware


//configuracion del servidor  para pasarlos en el parametro de socket
const server=http.createServer(aplicacion)
const io = socketio(server)
require('./socket')(io)

server.listen(aplicacion.get('port'),()=>{    //inicio de la aplicacion
    console.log('aplicacion corriendo en el puerto: ' +aplicacion.get('port'))
})